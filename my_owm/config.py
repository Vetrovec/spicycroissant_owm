import os

CONFIG_BASE = os.path.expanduser('~/.config/owm-spicycroissant')
CONFIG_KEY = os.path.join(CONFIG_BASE, 'key')
CONFIG_CITIES = os.path.join(CONFIG_BASE, 'cities')

def get_key():
    try:
        with open(CONFIG_KEY, 'r') as key:
            return key.read()
    except FileNotFoundError:
        return None

def set_key(value):
    os.makedirs(CONFIG_BASE, exist_ok=True)
    with open(CONFIG_KEY, 'w') as key:
        key.write(value)

def list_city():
    try:
        with open(CONFIG_CITIES, 'r') as cities:
            return cities.read().splitlines()
    except FileNotFoundError:
        return list()

def add_city(value):
    os.makedirs(CONFIG_BASE, exist_ok=True)
    with open(CONFIG_CITIES, 'a') as cities:
        cities.write(value + '\n')

def remove_city(value):
    city_list = list_city()
    if len(city_list) < 1:
        return
    os.makedirs(CONFIG_BASE, exist_ok=True)
    with open(CONFIG_CITIES, 'w') as cities:
        for city in city_list:
            if city == str(value):
                continue
            cities.write(city + '\n')
