import gzip
import json
import os
import requests

CACHE_BASE = os.path.expanduser('~/.cache/nswi177-example/')
CACHE_CITY_LIST_JSON = os.path.join(CACHE_BASE, 'cities.json')

def get_city_list():
    try:
        with open(CACHE_CITY_LIST_JSON, 'r') as cities:
            return json.loads(cities.read())
    except FileNotFoundError:
        result = requests.get('http://bulk.openweathermap.org/sample/city.list.json.gz')
        content = gzip.decompress(result.content).decode('utf-8')
        os.makedirs(CACHE_BASE, exist_ok=True)
        with open(CACHE_CITY_LIST_JSON, 'w') as cities:
            cities.write(content)
        return json.loads(content)
