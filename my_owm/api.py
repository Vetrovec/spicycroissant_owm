import requests

class API:
    def __init__(self, key):
        self._key = key

    def get_current_weather(self, id):
        url = 'http://api.openweathermap.org/data/2.5/weather?id={id}&units=metric&appid={key}'
        result = requests.get(url.format(id=id, key=self._key)).json()
        current = {
            'temperature': round(result['main']['temp']),
            'description': result['weather'][0]['description']
        }
        return current

    def get_forecast(self, id):
        url = 'http://api.openweathermap.org/data/2.5/forecast?id={id}&units=metric&appid={key}'
        result = requests.get(url.format(id=id, key=self._key)).json()
        weather_list = result['list']
        weather_list = list(filter(lambda e: e['dt'] % 86400 == 43200, weather_list))
        weather_list.sort(reverse=False, key=lambda e: e['dt'])
        weather_list = list(
            map(
                lambda e: {
                    'date': e['dt_txt'].split(' ')[0],
                    'temperature': round(e['main']['temp']),
                    'description': e['weather'][0]['description']
                },
                weather_list,
            )
        )
        return weather_list
