from argparse import ArgumentParser
import sys
from my_owm.api import API
from my_owm.caching import get_city_list
import my_owm.config as config

# Add list of commands with descriptions
help_message = """my_owm [command] [<args>]"""

def register_handler():
    parser = ArgumentParser()
    parser.add_argument("key")
    args = parser.parse_args(sys.argv[2:])
    config.set_key(args.key)

def search_handler():
    parser = ArgumentParser()
    parser.add_argument("city")
    args = parser.parse_args(sys.argv[2:])
    city_list = get_city_list()
    city_list = list(filter(lambda e: args.city.lower() in e['name'].lower(), city_list))
    for city in city_list:
        print('{id} {country} {name}'.format(id=city['id'], name=city['name'], country=city['country']))

def add_handler():
    parser = ArgumentParser()
    parser.add_argument("id")
    args = parser.parse_args(sys.argv[2:])
    # TODO: Validate city id
    config.add_city(args.id)

def list_handler():
    # TODO: Optimize
    city_list = get_city_list()
    for id in config.list_city():
        id = id.strip()
        for data in city_list:
            if str(data['id']) == id:
                print('{id}: {city} ({country})'.format(id=data['id'], city=data['name'], country=data['country']))
                break

def rm_handler():
    parser = ArgumentParser()
    parser.add_argument("id")
    args = parser.parse_args(sys.argv[2:])
    config.remove_city(args.id)

def default_handler():
    # TODO: Missing key
    api = API(config.get_key())
    # TODO: Optimize
    city_list = get_city_list()
    for id in config.list_city():
        for data in city_list:
            if str(data['id']) == id:
                city_name = data['name']
                break
        current = api.get_current_weather(id)
        forecast = api.get_forecast(id)
        print('{city}: {description}, {temperature}°C'.format(
            city=city_name,
            description=current['description'],
            temperature=current['temperature']
            )
        )
        for data in forecast:
            print(' {date}: {description}, {temperature}°C'.format(
                date=data['date'],
                description=data['description'],
                temperature=data['temperature']
                )
            )
        print()

handlers = {
    "register": register_handler,
    "search": search_handler,
    "add": add_handler,
    "list": list_handler,
    "rm": rm_handler,
}

def main():
    if len(sys.argv) < 2:
        default_handler()
        return
    # Add descriptions to parser and arguments
    parser = ArgumentParser(
        usage=help_message,
    )
    parser.add_argument("command")
    args = parser.parse_args([sys.argv[1]])
    # Add unrecognized command
    handlers[args.command]()
