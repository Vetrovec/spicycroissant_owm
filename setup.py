from setuptools import setup

setup(
    name="spicycroissant_owm",
    description="OWM client",
    version="0.1",
    python_requires=">=3.6",
    packages=["my_owm"],
    entry_points={
        "console_scripts": [
            "my_owm = my_owm.main:main",
        ],
    },
)
